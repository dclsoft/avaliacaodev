# README #

AVALIAÇÃO .NET - SOLUTION SISTEMAS

### Instruções ###

* Crie um projeto em .NET para as questões 1 e 2. Lembre-se o foco dessa prova é o entendimento de sua lógica, portanto, não se preocupe com o front-end ou consistir informações em banco de dados e sim com a lógica para demonstrar as respostas das questões (um aplicativo de linha de comando ou uma página web simples apenas para executar os métodos, por exemplo).
* O não conhecimento de um ou mais tópicos não implica, necessariamente, em uma má avaliação geral.
* Após concluir, suba a versão em seu repositório do GitHub e compartilhe informe o link no e-mail de seu contato na Solution Sistemas

Boa sorte!


### Questão 1 ###

Uma revendedora de carros usados paga a seus funcionários vendedores um salário fixo por mês, mais uma comissão também fixa para cada carro vendido e mais 5% do valor das vendas por ele efetuadas.

Escrever um algoritmo que leia o número de carros por ele vendidos, o valor total de suas vendas, o salário fixo e o valor que ele recebe por carro vendido. Calcule e escreva o salário final do vendedor 


### Questão 2 ###

Uma companhia de telecomunicações criou o seu plano por minutos recentemente, em uma de suas modalidades de cobrança ela cobra seguindo as seguintes regras:

- Ela sempre cobra o primeiro minuto da chamada

- Após o primeiro minuto de chamada, ela cobra por frações de 1/10 de minuto, ou seja, a cada 6 segundos ela cobra 1/10 do valor do minuto


Por exemplo, se a companhia cobra, R$ 0,10 centavos por minuto:

- Uma chamada de 60 segundos assim como uma chamada de 10 segundos custará R$ 0,10

- Será cobrado R$ 0,01 centavos (ou seja, 1/10 do valor de 1 minuto) a cada 6 segundos após o primeiro minuto de chamada

- Uma chamada de 62 segundos custará R$ 0,11

- Uma chamada de 130 segundos custará R$ 0,22


Desenvolva uma rotina, que ao informar *a duração em segundos* e o *valor por minuto*, ele retorne ou mostre na tela o valor da chamada.